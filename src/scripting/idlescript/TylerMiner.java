package scripting.idlescript;

import com.openrsc.client.entityhandling.instances.Item;
import orsc.ORSCharacter;


public class TylerMiner extends IdleScript {
    private final int COPPER_ORE_1_ID = 100;
    private final int COPPER_ORE_2_ID = 101;
    private final int TIN_ORE_ID = 104;
    private final int IRON_ORE_ID = 102;
    private final int COAL_ORE_1_ID = 110;
    private final int DEPLETED_ORE_ID = 98;

    private final int BANKER_NPC_ID = 95;

    private boolean walkingToCoal = false;
    private boolean walkingToBank = false;
    private boolean depositingInventory = false;
    private boolean coalFound = false;

    private int[] coalPath = {
            222, 453,
            224, 459,
            224, 467,
            221, 474,
            223, 483,
            225, 494,
            226, 504,
    };
    private int[] bankPath = {
            226, 504,
            225, 494,
            223, 483,
            221, 474,
            224, 467,
            224, 459,
            222, 453
    };

    @Override
    public void start(String[] parameters) {
        if(inventoryIsEmpty() && depositingInventory) {
            depositingInventory = false;
        }

        if(controller.isBatching() && !inventoryIsFull()) {
            return;
        }

        coalFound = mineCoal();

        if(inventoryIsEmpty() && !depositingInventory && !coalFound) {
            walkToCoal();
            return;
        }

        if(inventoryIsFull()) {
          walkToBank();
          return;
        }
    }

    private boolean mineCoal() {
        if(inventoryIsFull()) {
            return false;
        }

        boolean found = mineOre(COAL_ORE_1_ID);

        int[] depletedOreCoords = controller.getNearestObjectById(DEPLETED_ORE_ID);
        if(depletedOreCoords != null && depletedOreCoords[0] != -1) {
            found = true;
        }

        if(found && walkingToCoal) {
            walkingToCoal = false;
        }

        return found;
    }

    private void walkToCoal() {
        if(walkingToCoal) {
            return;
        }

        controller.log("Walking to coal!", "cya");
        walkingToCoal = true;
        controller.walkPath(coalPath);
    }

    private void walkToBank() {
        ORSCharacter banker = controller.getNearestNpcById(BANKER_NPC_ID, false);
        if(banker != null && !walkingToBank) {
            depositInventory();
            return;
        }

        if(walkingToBank) {
            return;
        }

        controller.log("Walking to bank!", "cya");
        walkingToBank = true;
        controller.walkPath(bankPath);
        walkingToBank = false;
    }

    private void depositInventory() {
        if(controller.isInBank()) {
            controller.log("Depositing inventory!", "cya");
            depositingInventory = true;
            while(controller.getInventoryItems().size() != 0) {
                Item item = controller.getInventoryItems().get(0);
                int itemId = item.getItemDef().id;
                int itemAmount = controller.getInventoryItemCount(itemId);

                controller.depositItem(itemId, itemAmount);
                controller.sleep(500);
            }
            controller.closeBank();
            depositingInventory = false;
            controller.sleep(500);
            return;
        }

        controller.log("Found banker! Banking...", "cya");
        controller.openBank();
    }

    private boolean mineOre(int oreId) {
        int[] oreCoords = controller.getNearestObjectById(oreId);
        if(oreCoords != null && oreCoords[0] != -1) {
            controller.log("Mining ore!", "cya");
            controller.atObject(oreCoords[0], oreCoords[1]);
            return true;
        } else {
            controller.log("No ore found!");
        }

        return false;
    }

    private int getDistanceFromLocalPlayer(int x, int z) {
        if(x == -1 || z == -1) {
            return -1;
        }

        x = controller.offsetX(x);
        z = controller.offsetZ(z);

        return controller.getDistanceFromLocalPlayer(x, z);
    }

    private boolean inventoryIsEmpty() {
        return controller.getInventoryItemCount() == 0;
    }

    private boolean inventoryIsFull() {
        return controller.getInventoryItemCount() == 30;
    }
}
