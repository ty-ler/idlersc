package scripting.idlescript;

import orsc.ORSCharacter;

public class TylerFlaxer extends IdleScript {
    private final int FLAX_OBJECT_ID = 313;
    private final int FLAX_ITEM_ID = 675;
    private final int BANKER_NPC_ID = 95;

    private int[] flaxPath = new int[] {
            501, 452,
            503, 467,
            503, 472,
            496, 480,
            491, 485
    };

    private int[] bankPath = new int[] {
            491, 485,
            496, 480,
            502, 472,
            502, 462,
            501, 452,
    };

    private boolean foundFlax = false;
    private boolean walkingToBank = false;
    private boolean walkingToFlax = false;

    @Override
    public void start(String[] parameters) {

        if(!walkingToBank && !walkingToFlax) {
            if(!controller.isBatching()) {
                if(isInventoryFull()) {
                    goBank();
                } else {
                    foundFlax = controller.atObject(FLAX_OBJECT_ID);

                    if(foundFlax) {
                        controller.log("Picking flax!", "gr3");

                        if(walkingToFlax) {
                            walkingToFlax = false;
                        }
                    } else {
                        walkToFlax();
                    }
                }
            }
        } else if(walkingToBank && isInventoryFull()) {
            goBank();
        }
    }

    private void walkToFlax() {
        if(walkingToFlax || walkingToBank) {
           return;
        }

        controller.log("Walking to flax!", "cya");
        walkingToFlax = true;
        controller.walkPath(flaxPath);
        walkingToFlax = false;
    }

    private void walkToBank() {
        if(walkingToBank || walkingToFlax) {
            return;
        }


        controller.log("Walking to bank!", "cya");
        walkingToBank = true;
        controller.walkPath(bankPath);
    }

    private void depositInventory() {
        if(controller.isInBank()) {
            controller.log("Depositing flax!", "cya");
            int flaxInventoryAmount = controller.getInventoryItemCount(FLAX_ITEM_ID);
            controller.depositItem(FLAX_ITEM_ID, flaxInventoryAmount);
            controller.sleep(500);
            controller.closeBank();
            controller.sleep(250);
            walkingToBank = false;
            return;
        }

        controller.log("Found the banker, opening bank!", "cya");
        controller.openBank();
    }

    private void goBank() {
        if(isBankerNearby()) {
            depositInventory();
        } else {
            walkToBank();
        }
    }

    private boolean isBankerNearby() {
        ORSCharacter banker = controller.getNearestNpcById(BANKER_NPC_ID, false);

        return banker != null;
    }

    private boolean isInventoryFull() {
        return controller.getInventoryItemCount() == 30;
    }
}
