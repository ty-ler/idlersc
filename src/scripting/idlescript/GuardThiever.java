package scripting.idlescript;

import orsc.ORSCharacter;

public class GuardThiever extends IdleScript {
    private final int GUARD_NPC_ID = 65;
    private final int TEA_SELLER_NPC_ID = 780;
    private final int BANKER_NPC_ID = 95;
    private final int TEA_ITEM_ID = 739;
    private final int GOLD_ITEM_ID = 10;

    private int minHealth = 15;
    private int thresholdHealth = 35;
    private boolean healingToThreshold = false;

    private int goldThreshold = 1000000;

    private int[] originalCoords = null;
    private boolean returningToOriginalCoords = false;

    private ORSCharacter thievingGuard = null;

    @Override
    public void start(String[] parameters) {
        if(controller.isLoggedIn() && originalCoords == null) {
            originalCoords = new int[] { controller.getPlayer().currentX, controller.getPlayer().currentZ };
        }

        if(getPlayerCurrentHealth() >= thresholdHealth) {
            healingToThreshold = false;
        }

        if(controller.isInCombat()) {
            attemptEscapeFromCombat();
            return;
        }

        if(getPlayerCurrentGold() >= goldThreshold) {
            depositGold();
            return;
        }

        if(healingToThreshold && canDrinkTea()) {
            controller.log("Drinking tea...", "gr2");
            controller.itemCommand(TEA_ITEM_ID);
            controller.sleep(500);
            return;
        } else if(healingToThreshold && !canDrinkTea()) {
            buyTeas();
        }

        if(getPlayerCurrentHealth() <= minHealth) {
            if(canDrinkTea()) {
                healingToThreshold = true;
                return;
            }


            controller.log("Below minimum health, buying tea!");
            buyTeas();
            return;
        }

        if(canThieve()) {
            thieveGuard();
        }
    }

    public void thieveGuard() {
        if(thievingGuard != null) {
             int[] thievingGuardCoords = controller.getNpcCoordsByServerIndex(thievingGuard.serverIndex);

             if(thievingGuardCoords[0] != -1) {
                 _thieveGuard();
             } else {
                 thievingGuard = null;
             }
        } else {
            ORSCharacter nearestGuard = controller.getNearestNpcById(GUARD_NPC_ID, false);

            if(nearestGuard != null) {
                thievingGuard = nearestGuard;
                _thieveGuard();
            } else {
                controller.log("No Guards found!");
            }
        }
    }

    private void _thieveGuard() {
        controller.log("Thieving Guard!", "gr3");
        controller.thieveNpc(thievingGuard.serverIndex);
        controller.sleep(2000);
    }

    private void attemptEscapeFromCombat() {
        controller.log("Attempting to escape combat!");
        this.controller.walkTo(this.controller.currentX(), this.controller.currentY(), 0, true);
    }

    private void depositGold() {
        controller.log("Depositing gold into bank!", "cya");

        if(controller.isInBank()) {
            controller.depositItem(GOLD_ITEM_ID, getPlayerCurrentGold());
            controller.sleep(500);
            controller.closeBank();
            return;
        }

        if(bankerIsNearby()) {
            controller.openBank();
        } else {
            returnToOriginalCoords();
        }
    }

    private void returnToOriginalCoords() {
        if(originalCoords == null) {
            return;
        }

        controller.log("Couldn't find any bankers, returning to starting location...");
        returningToOriginalCoords = true;
        controller.walkTo(originalCoords[0], originalCoords[1], 10, true);
    }

    private void buyTeas() {
        controller.log("Buying teas!", "gr2");
        if(controller.isInShop()) {
            int teaCount = controller.getShopItemCount(TEA_ITEM_ID);

            if(teaCount > 0) {
                controller.shopBuy(TEA_ITEM_ID, 20);
                controller.sleep(1000);
            }

            controller.closeShop();
            return;
        }

        ORSCharacter teaSeller = controller.getNearestNpcById(TEA_SELLER_NPC_ID,false);

        if(teaSeller != null) {
            controller.npcCommand1(teaSeller.serverIndex);
        }
    }

    private boolean canThieve() {
        return !controller.isInCombat() && getPlayerCurrentHealth() > minHealth && !healingToThreshold && !controller.isBatching();
    }

    private boolean canDrinkTea() {
        return controller.isItemInInventory(TEA_ITEM_ID);
    }

    private boolean bankerIsNearby() {
        ORSCharacter banker = controller.getNearestNpcById(BANKER_NPC_ID, false);

        return banker != null;
    }

    private int getPlayerCurrentGold() {
        return controller.getInventoryItemCount(GOLD_ITEM_ID);
    }

    private int getPlayerCurrentHealth() {
        return controller.getCurrentStat(controller.getStatId("Hits"));
    }
}
