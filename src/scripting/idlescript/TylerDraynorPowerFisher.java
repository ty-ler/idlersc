package scripting.idlescript;

public class TylerDraynorPowerFisher extends IdleScript {
    private final int SHRIMP_SPOT_ID = 193;

    @Override
    public void start(String[] parameters) {
        if(controller.isInCombat()) {
            escapeCombat();
        } else if(!controller.isBatching()) {
            fishShrimp();
        }
    }

    private void fishShrimp() {
        int[] shrimpSpotCoords = controller.getNearestObjectById(SHRIMP_SPOT_ID);

        if(shrimpSpotCoords != null && shrimpSpotCoords[0] != -1) {
            controller.log("Fishing at shrimp spot!", "gr3");
            controller.atObject(shrimpSpotCoords[0], shrimpSpotCoords[1]);
        }
    }

    private void escapeCombat() {
        controller.log("Escaping combat!");
        this.controller.walkTo(this.controller.currentX(), this.controller.currentY(), 0, true);
    }
}
