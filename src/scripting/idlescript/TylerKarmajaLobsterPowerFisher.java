package scripting.idlescript;

public class TylerKarmajaLobsterPowerFisher extends IdleScript {
    private final int LOBSTER_SPOT_ID = 194;

    @Override
    public void start(String[] parameters) {
        if(!controller.isBatching()) {
            controller.log("Fishing at lobster spot!");
            controller.atObject2(LOBSTER_SPOT_ID);
        }
    }
}
