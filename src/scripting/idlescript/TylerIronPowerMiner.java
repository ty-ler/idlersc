package scripting.idlescript;

public class TylerIronPowerMiner extends IdleScript {
    private final int IRON_ROCK_OBJ_ID = 102;


    @Override
    public void start(String[] parameters) {
        if(!controller.isBatching()) {
            mineOre(IRON_ROCK_OBJ_ID);
        }
    }

    private boolean mineOre(int oreId) {
        int[] oreCoords = controller.getNearestObjectById(oreId);
        if(oreCoords != null && oreCoords[0] != -1) {
            controller.log("Mining ore!", "cya");
            controller.atObject(oreCoords[0], oreCoords[1]);
            return true;
        } else {
            controller.log("No ore found!");
        }

        return false;
    }
}
