package scripting.idlescript;

import com.openrsc.client.entityhandling.EntityHandler;
import com.openrsc.client.entityhandling.defs.PrayerDef;
import orsc.ORSCharacter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MonkKiller extends IdleScript {
    private final int MONK_ID = 93;
    private final int BONES_ID = 20;
    private final int ALTAR_ID = 19;
    private final int MONKS_ALTAR_ID = 200;

    private final int TICK_TIME = 618;

    private boolean setup = false;

    private int minHealth = 6;
    private int thresholdHealth = 18;
    private boolean healingToThreshold = false;

    private ORSCharacter attackingMonk = null;

    private boolean usePrayer = true;
    private boolean praying = false;
    private String[] prayerNames = new String[] { "Burst of strength", "Clarity of thought", "Superhuman strength", "Improved reflexes", "Ultimate strength", "Incredible reflexes" };
    private List<PrayerDef> availablePrayers = new ArrayList();

    public void start(String[] parameters) {
        if(!setup) {
            setupScript(parameters);
        }

        resetAttackingMonkWhenNeeded();

        if(getLocalPlayerPrayer() == 0 && praying) {
            praying = false;
        }

        if(controller.isInOptionMenu() || needToHeal()) {
            attemptHealWithMonk();
        } else if(!controller.isInCombat()) {
            disableAllPrayers();
            boolean needToPickupBones = attemptPickUpBones();
            boolean needToBuryBones = attemptBuryBones();
            boolean needPrayAtAltar = needToPrayAtAltar();

            if(!needToPickupBones && !needToBuryBones) {
                if(needPrayAtAltar) {
                    attemptPrayAtAltar();
                } else {
                    attackMonk();
                }
            }
        } else if(controller.isInCombat()) {
            if(usePrayer) {
                enablePrayers();
            }
        }
    }

    private void setupScript(String[] parameters) {
        if(parameters.length >= 2) {
            minHealth = Integer.parseInt(parameters[0]);
            thresholdHealth = Integer.parseInt(parameters[1]);
        }

        setup = true;
    }

    private void attackMonk() {
        ORSCharacter nearestMonk = controller.getNearestNpcById(MONK_ID, false);

        if(attackingMonk != null) {
            int[] attackingMonkCoords = controller.getNpcCoordsByServerIndex(attackingMonk.serverIndex);
            if(attackingMonkCoords[0] != -1) {
                controller.log("Attacking Monk!", "red");
                controller.attackNpc(attackingMonk.serverIndex);
            } else {
                attackingMonk = null;
            }
        } else {
            if(nearestMonk != null) {
                controller.log("Attacking Monk!", "red");
                controller.attackNpc(nearestMonk.serverIndex);
                attackingMonk = nearestMonk;
            }
        }
    }

    private boolean attemptBuryBones() {
        if(controller.getInventoryItemCount(BONES_ID) <= 0) {
            return false;
        }

        controller.log("Burying bones...", "cya");
        controller.itemCommand(BONES_ID);
        controller.sleep(TICK_TIME);
        return true;
    }

    private boolean attemptPickUpBones() {
        if(controller.isInOptionMenu() || controller.isInCombat() || controller.getInventoryItemCount() == 30) {
            return false;
        }

        int[] nearestBonesCoords = controller.getNearestItemById(BONES_ID);
        if(nearestBonesCoords != null) {
            controller.log("Picking up bones!", "cya");
            controller.pickupItem(nearestBonesCoords[0], nearestBonesCoords[1], BONES_ID, true, true);

            return true;
        }

        return false;
    }

    private void attemptPrayAtAltar() {
        int[] altarCoords = controller.getNearestObjectById(ALTAR_ID);
        int[] monkAltarCoords = controller.getNearestObjectById(MONKS_ALTAR_ID);

        if(altarCoords != null && altarCoords[0] != -1) {
            controller.log("Praying at the altar!", "cya");
            controller.atObject(altarCoords[0], altarCoords[1]);
        } else if(monkAltarCoords != null && monkAltarCoords[0] != -1) {
            controller.log("Praying at the altar!", "cya");
            controller.atObject(monkAltarCoords[0], monkAltarCoords[1]);
        }
    }

    private void attemptHealWithMonk() {
        if(controller.isInCombat()) {
            attemptEscapeFight();
            return;
        }

        disableAllPrayers();

        if(!healingToThreshold) {
            healingToThreshold = true;
        }
        ORSCharacter nearestMonk = controller.getNearestNpcById(MONK_ID, false);

        if(nearestMonk != null) {
            controller.log("Healing on Monk to threshold!", "cya");

            if(!controller.isInOptionMenu()) {
                controller.talkToNpc(nearestMonk.serverIndex);
                controller.sleep(3000);
            } else {
                controller.optionAnswer(0);
                controller.sleep(8000);

                if(getLocalPlayerHealth() >= thresholdHealth) {
                    healingToThreshold = false;
                }
            }
        } else {
            // no monks found

        }
    }

    private void attemptEscapeFight() {
        controller.log("Attempting to escape fight!", "ora");
        this.controller.walkTo(this.controller.currentX(), this.controller.currentY(), 0, true);
    }

    private boolean needToHeal() {
        int localPlayerHealth = getLocalPlayerHealth();
        int localPlayerMaxHealth = getLocalPlayerMaxHealth();

        return localPlayerHealth <= minHealth || healingToThreshold;
//        return localPlayerHealth <= Math.max((Math.floor(localPlayerMaxHealth * .5)), 5);
    }

    private boolean needToPrayAtAltar() {
        int localPlayerPrayer = getLocalPlayerPrayer();

        return localPlayerPrayer == 0;
    }

    private int getLocalPlayerHealth() {
        return controller.getCurrentStat(controller.getStatId("Hits"));
    }

    private int getLocalPlayerMaxHealth() {
        return controller.getBaseStat(controller.getStatId("Hits"));
    }

    private int getLocalPlayerPrayer() {
        return controller.getCurrentStat(controller.getStatId("Prayer"));
    }

    private int getLocalPlayerMaxPrayer() {
        return controller.getBaseStat(controller.getStatId("Prayer"));
    }

    private void getAvailablePrayers() {
        availablePrayers.clear();

        for(String prayerName : prayerNames) {
            int prayerId = controller.getPrayerId(prayerName);
            int prayerLevel = getLocalPlayerMaxPrayer();

            PrayerDef prayerDef = EntityHandler.getPrayerDef(prayerId);
            prayerDef.id = prayerId;

            if(prayerDef.getReqLevel() <= prayerLevel) {
                availablePrayers.add(prayerDef);
            }
        }
    }

    private void resetAttackingMonkWhenNeeded() {
        if(attackingMonk == null) {
            return;
        }

        int[] attackingMonkCoords = controller.getNpcCoordsByServerIndex(attackingMonk.serverIndex);
        boolean monkIsInCombat = controller.isNpcInCombat(attackingMonk.serverIndex);
        if(attackingMonkCoords[0] == -1 || monkIsInCombat) {
            attackingMonk = null;
        }
    }

    private void enablePrayers() {
        getAvailablePrayers();

        if(availablePrayers.size() > 0 && !praying && getLocalPlayerPrayer() > 0) {
            controller.log("Enabling prayers!", "gr3");
            praying = true;

            if(availablePrayers.size() == 1) {
                controller.enablePrayer(availablePrayers.get(0).id);
            } else {
                PrayerDef p1 = availablePrayers.get(availablePrayers.size() - 1);
                PrayerDef p2 = availablePrayers.get(availablePrayers.size() - 2);

                controller.enablePrayer(p1.id);
                controller.enablePrayer(p2.id);
            }
        }
    }

    private void disableAllPrayers() {
        if(!praying) {
            return;
        }

        controller.log("Disabling prayers!", "or1");
        praying = false;

        for(PrayerDef prayerDef : availablePrayers) {
            controller.disablePrayer(prayerDef.id);
        }
    }
}
